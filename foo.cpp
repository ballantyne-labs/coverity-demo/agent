/*
 * This is terrible code for testing SAST 
 *
 */

#include <stdio.h>
#include <string.h>
#include <vector>
#include <string>

using namespace std;

#define MY_STRING_MAX 16


int
tokenise( const char * src, char delim, vector<string> & list )
{
    char tmp[MY_STRING_MAX];
    char *begin = tmp;
    int count = 1;
    strncpy(tmp,src,strlen(src));

    for ( int i = 0; i < strlen(src); i++)
    {
        if ( tmp[i] == delim )
        {
            tmp[i] = '\0';

            list.push_back(begin);
            begin = tmp+1;
            count++;
        }
    }

    list.push_back(begin);

    return count;
}

int
main( int argc, char * argv[] )
{
    vector<string> tokenlist;

    int numtokens = tokenise( argv[1],argv[2][0],tokenlist);

    printf( "found %s tokens\n");

    return 0;
}

